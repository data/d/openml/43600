# OpenML dataset: Updated-Wine-Enthusiast-Reviews

https://www.openml.org/d/43600

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
I built it on https://www.kaggle.com/zynicide/wine-reviews updating the scraper fetching fresh data.
Content
All wine reviews from winemag.com for 2017-2020 years. Duplicates are cleared, vintage parsed from title. NV means non-vintage.
Acknowledgments
Thanks Zack Thoutt for the base scraper and original data

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43600) of an [OpenML dataset](https://www.openml.org/d/43600). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43600/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43600/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43600/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

